#ifndef _SDDCP_H_
#define _SDDCP_H_

#define SDDCP_MAX_PAYLOAD_NAME_LEN      16
#define SDDCP_MAX_ATTRIBUTE_KEY_LEN     16
#define SDDCP_MAX_ATTRIBUTES             4
#define SDDCP_MAX_ATTRIBUTE_VALUE_LEN   16

const char	*sddcp_announce_str  = "ann";
const char      *sddcp_service_str   = "svc";
const char	*sddcp_command_str   = "cmd";
const char      *sddcp_com_str       = "com";
const char	*sddcp_ack_str	     = "ack";
const char	*sddcp_nak_str	     = "nak";
const char	*sddcp_query_str     = "qry";
const char      *sddcp_response_str  = "rsp";

enum sddcp_msg_type {
	SDDCP_NONE,
	SDDCP_ANNOUNCE,
	SDDCP_COMMAND,
	SDDCP_ACK,
	SDDCP_NAK,
	SDDCP_QUERY,
};

struct sddcp_attribute {
	char key[SDDCP_MAX_ATTRIBUTE_KEY_LEN];
	char value[SDDCP_MAX_ATTRIBUTE_VALUE_LEN];
};

struct sddcp_payload {
	char name[SDDCP_MAX_PAYLOAD_NAME_LEN];
	uint8_t attr_count;
	struct sddcp_attribute attributes[SDDCP_MAX_ATTRIBUTES];
};

struct sddcp_message {
	enum sddcp_msg_type type;

        /* These are one byte larger than necessary to enable 0
	 * terminated strings */     
	char from[17];
	char to[17];
	
	struct sddcp_payload payload;
};

const char *sddcp_ver = "sddcp1";
#endif
