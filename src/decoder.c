#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "sddcp.h"

static int sddcp_get_field_len(char *el, int max_len, char endel)
{
	int i = 0;
	while(i < max_len && (el[i] != endel && el[i - 1] != '\\')) {
		i++;
	}

	if (el[i] == endel)
		return i;

	return -1;
}

/* Returns the length of the decoded address element, returns -1 on error */
static int sddcp_decode_addresses(char *ad, char *from, char *to)
{
	int i = 1;
	int len = sddcp_get_field_len(ad, 32, ')');

	if (len < 0) {
		printf("Got bad address length\n");
		return len;
	}

	/* Copy the from address */
	while(i < len && ad[i] != '|') {
		*(from++) = ad[i++];
	}

	/* Make sure we're at the delimiter */
	if (ad[i++] != '|') {
		printf("Missing delimiter\n");
		return -1;
	}

	/* Copy the 'to' address */
	while(i < len && ad[i] != ')') {
		*(to++) = ad[i++];
	}

	/* Make sure we're at the end of the address field */
	if (ad[i] != ')') {
		printf("Missing end of address\n");
		printf("Found %c instead\n", ad[i]);
		return -1;
	}

	/* Return the length of the decoded address */
	return len;

}

/* Returns the attribute length, or negative error */
static int sddcp_get_attribute_value_len(char *at, int max_len)
{
	int i = 0;

	while (i < max_len){
		if(at[i] == '>' && at[i - 1] != '\\') {
			return i;
		} else if (at[i] == '|' && at[i - 1] != '\\') {
			return i;
		}
		i++;
	}

	return -1;
}

/* Returns the length of the attribute, or a negative error */
static int sddcp_decode_single_attr(char *at, int max_len,
				    struct sddcp_attribute *dst)
{
	int i = 0;
	int ret, len;

	/* Ensure we have a '[' denoting the attribute name and
	 * increment the iterator */
	if (at[i] != '[') {
		printf("Missing attribute start\n");
		printf("expected [, but got %c at position %d\n",
		       at[i], i);
		printf("attribute: %s", at);
		return -1;
	}
	i++;

	/* Get the field name */
	len = sddcp_get_field_len(&at[i], max_len - i, ']');


	if (len < 0) {
		printf("Field has no length\n");
		return len;
	}

	memcpy(dst->key, &at[i], len);

	/* Move the iterator to the end of the bracket and check for
	 * the colon*/
	i += len + 1;

	if(at[i++] != ':') {
		printf("Missing colon\n");
		return -1;

	}

	/* Check the value length */
	len = sddcp_get_attribute_value_len(&at[i], max_len - i);

	if (len < 0) {
		printf("Invalid attribute length\n");
		return len;
	}


	/* Copy in the value */
	memcpy(dst->value, &at[i], len);

	i += len;

	return i;
}

static int sddcp_decode_attributes(char *at, int max_len,
				   struct sddcp_payload *dst)
{
	int i = 0;
	int ret;

	while (i < max_len && at[i] != '>') {
		ret = sddcp_decode_single_attr(&at[i], max_len - i,
					      &dst->attributes[dst->attr_count++]);
		if (ret < 0)
			return ret;
		i += ret;

		/* If there is a second attribute, advance the iterator */
		if (at[i] == '|')
			i++;
	}

	return 0;
}

/* REturns length of payload string, or negative error */
static int sddcp_verify_payload_type(char *pl, enum sddcp_msg_type type)
{
	int ret = 0;
	switch (type) {
	case SDDCP_NONE:
		return -1;
	case SDDCP_ANNOUNCE:
		ret = strncmp(pl, sddcp_service_str, strlen(sddcp_announce_str));
		if (ret)
			return -1;
		return strlen(sddcp_announce_str);
	case SDDCP_COMMAND:
		ret = strncmp(pl, sddcp_com_str, strlen(sddcp_command_str));
		if (ret)
			return -1;
		return strlen(sddcp_command_str);
	case SDDCP_ACK:
	case SDDCP_NAK:
		ret = strncmp(pl, sddcp_response_str, strlen(sddcp_response_str));
		if (ret)
			return -1;
		return strlen(sddcp_response_str);
	case SDDCP_QUERY:
		ret = strncmp(pl, sddcp_service_str, strlen(sddcp_query_str));
		if (ret)
			return -1;
		return strlen(sddcp_query_str);
	}

	return -1;
}

static int sddcp_decode_payload(char *pl, int max_len,
				struct sddcp_payload *dst,
				enum sddcp_msg_type type)
{
	int i = 1;
	int j = 0;
	int ret = 0;
	int len;

	/* Clear the target payload */
	memset(dst, 0x00, sizeof(struct sddcp_payload));

	/* Ensure that the payload matches up */
	ret = sddcp_verify_payload_type(&pl[1], type);

	if (ret < 0) {
		printf("Invalid Payload\n");
		printf("%s\n", pl);
		return -1;
	}

	i += ret;

	/* Check for the colon */
	if (pl[i++] != ':') {
		printf("missing colon\n");
		return -1;
	}

	/* Copy the name to the service destination */
	len = sddcp_get_field_len(&pl[i],
				  SDDCP_MAX_PAYLOAD_NAME_LEN,
				  '|');

	if (len > 0) {
		/* This means there are attributes */

		/* Copy the name into it's new location*/
		memcpy(dst->name, &pl[i], len);
		i += len + 1;

		ret = sddcp_decode_attributes(&pl[i], max_len - i, dst);
	} else {
		len = sddcp_get_field_len(&pl[i],
					  SDDCP_MAX_PAYLOAD_NAME_LEN,
					  '>');
		memcpy(dst->name, &pl[i], len);
	}



	return ret;
}

static int sddcp_decode_fields(char *pk, int len,
			       struct sddcp_message *msg,
			       enum sddcp_msg_type type)
{

	int i = 1;
	int ret = 0;

	/* First things first, decode the addresses */

	ret = sddcp_decode_addresses(pk, msg->from, msg->to);

	/* If this was a bad decode, propegate the error */
	if (ret < 0) {
		printf("unable to decode addresses\n");
		return ret;
	}

	/* Move i to the end of the addresses */
	i += ret;

	/* Check for the '=' sign */
	if (pk[i++] != '=') {
		printf("missing equals sign\n");
		printf("Expected = at pos %d, but got %c", i - 1, pk[i - 1]);
		return -1;
	}

	/* Decode the payload */
	ret = sddcp_decode_payload(&pk[i], len - i, &msg->payload, type);

	return ret;
}

int sddcp_decode(char *pk, int len, struct sddcp_message *msg)
{
	int i = 0;
	int res = 0;

	/* Check that this is a full sddcp packet before
	 * moving on */
	if (!(pk[0] == '{' && pk[len - 1] == '}')) {
		printf("Invalid packet\n");
		return -1;
	}

	i = 1;

	/* check that the version matches */
	if (strncmp(&pk[1], sddcp_ver, strlen(sddcp_ver))) {
		printf("Invalid Version\n");
		return -2;
	}

	i += strlen(sddcp_ver);

	/* Check for the asterisk */
	if (pk[i++] != '*') {
		printf("Missing Asterisk\n");
		return -1;
	}

	/* Get the packet type */
	if(strncmp(&pk[i], sddcp_announce_str, strlen(sddcp_announce_str)) == 0) {
		/* Move the counter to after the 'ann' part of the string */
		i += strlen(sddcp_announce_str);

		msg->type = SDDCP_ANNOUNCE;
		/* Attempt to decode the announce packet */
		res = sddcp_decode_fields(&pk[i], len - i, msg, SDDCP_ANNOUNCE);

		return res;
	}else if(strncmp(&pk[i], sddcp_command_str,
			 strlen(sddcp_command_str)) == 0) {
		i += strlen(sddcp_command_str);

		msg->type = SDDCP_COMMAND;

		res = sddcp_decode_fields(&pk[i], len - i, msg, SDDCP_COMMAND);
	}else if(strncmp(&pk[i], sddcp_query_str,
			 strlen(sddcp_query_str)) == 0) {
			i += strlen(sddcp_query_str);

		msg->type = SDDCP_QUERY;

		res = sddcp_decode_fields(&pk[i], len - i, msg, SDDCP_QUERY);
	} else if (strncmp(&pk[i], sddcp_ack_str, strlen(sddcp_ack_str)) == 0) {
		msg->type = SDDCP_ACK;
		i += strlen(sddcp_ack_str);
		res = sddcp_decode_fields(&pk[i], len - i, msg, SDDCP_ACK);
	} else if (strncmp(&pk[i], sddcp_nak_str, strlen(sddcp_nak_str)) == 0) {
		msg->type = SDDCP_NAK;
		i += strlen(sddcp_nak_str);
		res = sddcp_decode_fields(&pk[i], len - i, msg, SDDCP_NAK);
	} else {
		msg->type = SDDCP_NONE;
		res = -1;
	}

	return res;
}

int main(void)
{
	int ret, i;
	char *ann = "{sddcp1*ann(192.168.32.1|~)=<svc:mqtt|[data]:temperature>}";
	char *qry = "{sddcp1*qry(192.168.32.1|~)=<svc:tftp>}";
	char *cmd = "{sddcp1*cmd(192.168.32.1|192.168.32.2)=<com:mqtt_st|[port]:1334>}";
	char *ack = "{sddcp1*ack(192.168.32.1|192.168.32.2)=<rsp:mqtt>}";
	char *nak = "{sddcp1*nak(192.168.32.1|192.168.32.1)=<rsp:mqtt>}";

	struct sddcp_message msg;
	memset(&msg, 0x00, sizeof(struct sddcp_message));

	//ret = sddcp_decode(ann, strlen(ann), &msg);
	//ret = sddcp_decode(qry, strlen(qry), &msg);
	//ret = sddcp_decode(cmd, strlen(cmd), &msg);
	ret = sddcp_decode(ack, strlen(ack), &msg);
	//ret = sddcp_decode(nak, strlen(nak), &msg);

	if (ret < 0) {
		printf("Unable to decode packet!\n");
		return ret;
	}

	switch (msg.type) {
	case SDDCP_ANNOUNCE:
		printf("Node: %s announced service:\n", msg.from);
		printf("<%s>\n", msg.payload.name);
		for(i = 0; i < msg.payload.attr_count; i++) {
			printf("-%s: %s\n", msg.payload.attributes[i].key,
			       msg.payload.attributes[i].value);
		}
		printf("To: ");
		if(msg.to[0] == '~')
			printf("All\n");
		else
			printf("%s\n", msg.to);
		break;
	case SDDCP_QUERY:
		printf("Node: %s has queried ", msg.from);
		if(msg.to[0] == '~')
			printf("All\n");
		else
			printf("%s ", msg.to);
		printf("for the following service:\n", msg.to);
		printf("<%s>", msg.payload.name);
		break;
	case SDDCP_COMMAND:
		printf("Node: %s has commanded ", msg.from);
		if(msg.to[0] == '~')
			printf("All\n");
		else
			printf("%s ", msg.to);
		printf("to: \n");
		printf("<%s>\n", msg.payload.name);
		for(i = 0; i < msg.payload.attr_count; i++) {
			printf("-%s: %s\n", msg.payload.attributes[i].key,
			       msg.payload.attributes[i].value);
		}
		break;
	case SDDCP_ACK:
		printf("Node %s has ACK'd %s\n", msg.from, msg.to);
		break;
	case SDDCP_NAK:
		printf("Node %s has NAK'd %s\n", msg.from, msg.to);
		break;
	case SDDCP_NONE:
		printf("Bogus Packet\n");
		break;
	}

	return 0;
}
