#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include "../inc/sddcp.h"
enum encoder_state {
	ENC_START_MSG,
	ENC_NAME,
	ENC_ADDR,
	ENC_START_PAYLOAD,
	ENC_ATR,
	ENC_FINISH_PAYLOAD,
	ENC_FINISH_MSG,
	ENC_DONE
};

static int sddcp_encode_fields(struct sddcp_message *msg,
			       char* buf,
			       int remaining_len,
			       enum encoder_state *state,
			       int *attrs)
{
	int ret;
	char *startpos = buf;
	struct sddcp_attribute *tmp_attr;

	switch (*state) {
	case ENC_START_MSG:
		*(buf++) = '{';
		strcpy(buf, sddcp_ver);
		buf += strlen(sddcp_ver);
		*(buf++) = '*';
		*state = ENC_NAME;
		ret = buf - startpos;
		break;
	case ENC_NAME:
		switch (msg->type) {
		case SDDCP_ANNOUNCE:
			strcpy(buf, sddcp_announce_str);
			buf += strlen(sddcp_announce_str);
			break;
		case SDDCP_COMMAND:
			strcpy(buf, sddcp_command_str);
			buf += strlen(sddcp_command_str);
			break;
		case SDDCP_ACK:
			strcpy(buf, sddcp_ack_str);
			buf += strlen(sddcp_ack_str);
			break;
		case SDDCP_NAK:
			strcpy(buf, sddcp_nak_str);
			buf += strlen(sddcp_nak_str);
			break;
		case SDDCP_QUERY:
			strcpy(buf, sddcp_query_str);
			buf += strlen(sddcp_query_str);
			break;
		case SDDCP_NONE:
			return -1;
		}
		*state = ENC_ADDR;
		ret = buf - startpos;
		break;
	case ENC_ADDR:
		*(buf++) = '(';

		/* Check for properly terminated strings */
		if(msg->from[16] != 0  || msg->to[16] != 0)
			return -1;

		strcpy(buf, msg->from);
		buf += strlen(msg->from);

		*(buf++) = '|';

		strcpy(buf, msg->to);
		buf += strlen(msg->to);

		*(buf++) = ')';
		*(buf++) = '=';
		*state = ENC_START_PAYLOAD;
		ret = buf - startpos;
		break;
	case ENC_START_PAYLOAD:
		*(buf++) = '<';
		/* Ensure properly terminated string */
		if (msg->payload.name[SDDCP_MAX_PAYLOAD_NAME_LEN - 1] != 0)
			return -1;

		switch (msg->type) {
		case SDDCP_ANNOUNCE:
		case SDDCP_QUERY:
			strcpy(buf, sddcp_service_str);
			buf += strlen(sddcp_service_str);
			break;
		case SDDCP_COMMAND:
			strcpy(buf, sddcp_com_str);
			buf += strlen(sddcp_com_str);
			break;
		case SDDCP_NAK:
		case SDDCP_ACK:
			strcpy(buf, sddcp_response_str);
			buf += strlen(sddcp_response_str);
			break;
		case SDDCP_NONE:
			return -1;
		}

		*(buf++) = ':';

		strcpy(buf, msg->payload.name);
		buf += strlen(msg->payload.name);

		/* Check for attributes */
		if(*attrs && !(msg->type == SDDCP_ACK || msg->type == SDDCP_NAK)) {
			*(buf++) = '|';
			*state = ENC_ATR;
		} else {
			*state = ENC_FINISH_PAYLOAD;
		}
		ret = buf - startpos;
		break;
	case ENC_ATR:
		*(buf++) = '[';

		/* Set up a pointer to the target attribute */
		tmp_attr = &msg->payload.attributes[msg->payload.attr_count - *attrs];

		/* Check for a properly terminated string */
		if (tmp_attr->key[SDDCP_MAX_ATTRIBUTE_KEY_LEN - 1] != 0)
			return -1;

		strcpy(buf, tmp_attr->key);
		buf += strlen (tmp_attr->key);

		*(buf++) = ']';

		*(buf++) = ':';

		/* Check for properly terminated string */
		if (tmp_attr->value[SDDCP_MAX_ATTRIBUTE_VALUE_LEN - 1] != 0)
			return -1;

		strcpy(buf, tmp_attr->value);
		buf += strlen (tmp_attr->value);

		/* Decrement the number of total attributes */
		*attrs -= 1;

		/* Check for attributes */
		if(*attrs) {
			*(buf++) = '|';
			*state = ENC_ATR;
		} else {
			*state = ENC_FINISH_PAYLOAD;
		}
		ret = buf - startpos;
		break;
	case ENC_FINISH_PAYLOAD:
		*(buf++) = '>';
		*state = ENC_FINISH_MSG;
		ret = buf - startpos;
		break;
	case ENC_FINISH_MSG:
		*(buf++) = '}';
		ret = buf - startpos;
		*state = ENC_DONE;
		break;
	case ENC_DONE:
		return 0;
		break;
	}

	return ret;
}

int sddcp_encode(struct sddcp_message *msg, char *buf, int buflen)
{
	enum encoder_state state = ENC_START_MSG;
	int pos = 0;
	int ret;
	int attrs = msg->payload.attr_count;

	if (msg->type == SDDCP_NONE)
		return -1;

	while (pos < buflen && state < ENC_DONE) {
		ret = sddcp_encode_fields(msg, &buf[pos], buflen - pos,
					  &state, &attrs);
		if (ret < 0)
			return ret;
		pos += ret;
	}

	if (pos > buflen || state != ENC_DONE)
		return -1;

	return 0;
}

int sddcp_set_addresses(struct sddcp_message *msg, const char *from,
			const char *to)
{
	/* Make sure we got a message and not some janky void pointer */
	if (!msg)
		return -1;

	/* Make sure we got strings */
	if (!from || !to)
		return -1;

	/* Check to be sure the strings aren't too long */
	if (strnlen(from, 17) > 16 || strnlen(to, 17) > 16)
		return -1;

	strcpy(msg->from, from);
	strcpy(msg->to, to);

	return 0;
}

int sddcp_set_payload_name(struct sddcp_message *msg, const char *pl_name)
{
	if (!msg || !pl_name)
		return -1;

	if (strnlen(pl_name, 17) > 16)
		return -1;

	strcpy(msg->payload.name, pl_name);
	return 0;
}

int sddcp_add_attribute(struct sddcp_message *msg, const char *key,
			const char *value)
{
	int attr_idx;
	
	if (!msg || !key || !value)
		return -1;

	if (strnlen(key, 17) > SDDCP_MAX_ATTRIBUTE_KEY_LEN ||
	   strnlen(value, 17) > SDDCP_MAX_ATTRIBUTE_VALUE_LEN)
		return -1;

	if (msg->payload.attr_count >= SDDCP_MAX_ATTRIBUTES)
		return -1;

	attr_idx = msg->payload.attr_count;

	strcpy(msg->payload.attributes[attr_idx].key, key);
	strcpy(msg->payload.attributes[attr_idx].value, value);

	msg->payload.attr_count++;
	return 0;
}

int main(void)
{
	int ret;

	char buf[128];
	struct sddcp_message msg;

	memset(&msg, 0x00, sizeof(struct sddcp_message));
	msg.type = SDDCP_ACK;

	sddcp_set_addresses(&msg, "192.168.32.2", "192.168.32.1");
	sddcp_set_payload_name(&msg, "mqtt");
	sddcp_add_attribute(&msg, "data", "temp");
	sddcp_add_attribute(&msg, "range", "0-256");

	ret = sddcp_encode(&msg, buf, sizeof(buf));

	if(ret == 0) {
		printf("%s\n", buf);
	} else {
		printf("Unable to encode\n");
		printf("%s\n", buf);
	}


	return 0;
}
