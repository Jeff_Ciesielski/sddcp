Small Device Discovery and Control Protocol
===========================================

A simple human and machine parseable protocol to enable network
service discovery and basic control for extremely resource limited
embedded devices

But wait, what about SSDP/UPnP?
-------------------------------

UPnP/SSDP (Simple Service Discovery Protocol) are rather ironically
named.  SSDP itself is easy enough to grok, however the use of HTTP
headers makes it rather difficult to parse on really tiny
microcontrollers, and control/events/etc requires XML parsing as well,
a feat which is a bit beyond what most people using an AVR want to
tackle.  Overall, this works fine in higher level languages on even
the tinyest of SOCs, but when you're working on bare metal and have to
fit a TCP stack + application logic + drivers + any required services
in flash, siplicity and functionality win out over full featuredness.



