#!/usr/bin/python

#The MIT License (MIT)
#
#Copyright (c) 2015 Jeff Ciesielski <jeffciesielski@gmail.com>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import socket
from Queue import Queue
from threading import Thread, Event
import struct
import os
import errno
from time import sleep
import traceback
import netifaces as ni
import traceback
import sys
import logging

logging.debug("")
_log = logging.getLogger('sddcp')
_log.setLevel(logging.INFO)

#TODO: Fix magic numbers

# Protocol definitions
VER = "sddcp1"

TYPE_NONE = None
TYPE_ANNOUNCE = "ann"
TYPE_COMMAND = "cmd"
TYPE_ACK = "ack"
TYPE_NAK = "nak"
TYPE_QUERY = "qry"

MESSAGE_TYPES = [TYPE_ANNOUNCE,
                 TYPE_COMMAND,
                 TYPE_ACK,
                 TYPE_NAK,
                 TYPE_QUERY]

SERVICE_STR = "svc"
COM_STR = "com"
RSP_STR = "rsp"

PAYLOAD_TYPE_MAP = {
    SERVICE_STR:[TYPE_ANNOUNCE, TYPE_QUERY],
    COM_STR:[TYPE_COMMAND]
    }

ADDR_ALL = "~"

# Server Definitions
SRV_PORT = 55555
SRV_MCAST_ADDR = '239.255.1.2'

def set_log_verbosity(v):
    _log.setLevel(v)

def get_interface_ip(iface):
    try:
        return ni.ifaddresses(iface)[2][0]['addr']
    except:
        return None

def get_local_ips():
    return filter(lambda x: x, map(get_interface_ip, ni.interfaces()))

class InvalidAttributeError(Exception):
    pass

class Attribute(object):
    def __init__(self, key, value):
        if len(key) > 32 or len(value) > 32:
            raise InvalidAttributeError()

        self.key = key
        self.value = value

    def __str__(self):
        return "key={} : value={}".format(self.key, self.value)

class Payload(object):
    def __init__(self, type_name, name):
        self.type_name = type_name
        self.name = name
        self.attributes = []

    def __str__(self):
        rst = "~payload~\n{} -- {}\n".format(self.type_name, self.name)
        for attr in self.attributes:
            rst += str(attr)

        return rst
        
    def get_attr_val(self, key):
        val = (x.value for x in self.attributes if x.key == key).next()
        return val

    def get_attr_list(self):
        return list(self.attributes)

    def append_attribute(self, attr):
        if not attr.__class__.__name__ == 'Attribute':
            print type(attr)
            raise TypeError
        self.attributes.append(attr)

class InvalidPacketError(Exception):
    pass

class _Message(object):
    def __init__(self, msg_type=TYPE_NONE,
                 payload_name = '',
                 to_addr=''):
        self.from_addr = ''
        self.to_addr = to_addr
        self._msg_type = msg_type
        pl_type_name = None

        if self._msg_type in  [TYPE_QUERY, TYPE_ANNOUNCE]:
            pl_type_name = SERVICE_STR
        elif self._msg_type == TYPE_COMMAND:
            pl_type_name = COM_STR
        elif self._msg_type in [TYPE_ACK, TYPE_NAK]:
            pl_type_name = RSP_STR

        if msg_type in [TYPE_ANNOUNCE,
                        TYPE_COMMAND, TYPE_QUERY]:
            if not len(payload_name):
                raise Exception("Invalid payload name: {}"\
                                .format(payload_name))

        self.payload = Payload(pl_type_name, payload_name)

    def append_payload_attribute(self, attr):
        if attr.__class__.__name__ != 'Attribute':
            print type(attr)
            raise TypeError
        self.payload.attributes.append(attr)

    @property
    def payload_name(self):
        return self.payload.name

    @property
    def payload_type(self):
        return self.payload.type_name

    def __str__(self):
        if not self._msg_type:
            raise InvalidPacketError()

        retstr = "{"
        retstr += VER + '*' + self._msg_type
        retstr += "({}|{})".format(self.from_addr, self.to_addr)

        retstr += "=<"

        retstr += "{}:{}".format(self.payload.type_name, self.payload.name)

        for attr in self.payload.attributes:
            retstr += "|[{}]:{}".format(attr.key, attr.value)

        retstr += ">}"
        return retstr

    def _validate_addresses(self):
        #a very simple test of IP addresses
        for addr in [self.from_addr, self.to_addr]:
            if addr == '~':
                continue

            socket.inet_aton(addr)

    def send(self):
        # Gather the address of each active network interface
        addresses = []
        for iface in ni.interfaces():
            try:
                addresses.append(ni.ifaddresses(iface)[2][0]['addr'])
            except:
                pass

        # Loop through each network interface and add it to the
        # multicast interface membership list.  In addition, create a
        # transmission socket and store it away
        for addr in addresses:
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM,
                                 socket.IPPROTO_UDP)
            sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 2)
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
            sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_IF, socket.inet_aton(addr))
            self.from_addr = addr
            sock.sendto(str(self), (SRV_MCAST_ADDR, SRV_PORT))
            sock.close()

class Announcement(_Message):
    def __init__(self, payload_name='', to_addr=''):
        super(Announcement, self).__init__(TYPE_ANNOUNCE, payload_name, to_addr)

class Command(_Message):
    def __init__(self, payload_name='', to_addr=''):
        super(Command, self).__init__(TYPE_COMMAND, payload_name, to_addr)

class Query(_Message):
    def __init__(self, payload_name='', to_addr=''):
        super(Query, self).__init__(TYPE_QUERY, payload_name, to_addr)

class Ack(_Message):
    def __init__(self, payload_name='', to_addr=''):
        super(Ack, self).__init__(TYPE_ACK, payload_name, to_addr)

class Nak(_Message):
    def __init__(self, payload_name='', to_addr=''):
        super(Nak, self).__init__(TYPE_NAK, payload_name, to_addr)

class MessageFactory(object):

    _msg_map = {
        TYPE_ANNOUNCE: Announcement,
        TYPE_COMMAND: Command,
        TYPE_ACK: Ack,
        TYPE_NAK: Nak,
        TYPE_QUERY: Query,
    }
    @staticmethod
    def make(msg_str):
        tmp_str = str(msg_str)

        #Validate that the packet is bookeneded by curly braces and
        #that it isn't too large
        if tmp_str[0] != '{' or tmp_str[-1] != '}':
            raise InvalidPacketError()

        if len(tmp_str) > 512:
            raise InvalidPacketError()

        #Strip off the outer brackets
        tmp_str = tmp_str[1:-1]

        #Make sure we understand this version
        if not tmp_str[:7] == "{}*".format(VER):
            raise InvalidPacketError()

        #strip off the version
        tmp_str = tmp_str[7:]

        #Make sure that this is a valid message type
        if not tmp_str[:3] in MESSAGE_TYPES:
            raise InvalidPacketError()

        _msg_type = tmp_str[:3]

        # Move to beginning of 'from address'
        tmp_str = tmp_str[3:]

        #Split out and validate the from and to addresses
        addstr = tmp_str.split("=")[0]

        if not addstr[0] == '(' and addstr[-1] == ')':
            raise InvalidPacketError()

        tmp_str = tmp_str[len(addstr):]
        addstr = addstr[1:-1]

        from_addr = addstr.split('|')[0]
        to_addr = addstr.split('|')[1]


        #Make sure we ended on a known boundary
        if not tmp_str[0] == '=':
            raise InvalidPacketError()

        #Strip out the equals sign
        tmp_str = tmp_str[1:]

        #We've arrived at the payload, ensure that it is bookeneded appropriately
        if tmp_str[0] != '<' or tmp_str[-1] != '>':
            raise InvalidPacketError()

        #Strip off the angle brackets
        tmp_str = tmp_str[1:-1]

        #Split the payload into the payload type/name and the payload attributes
        payload = tmp_str.split('|')

        msg = MessageFactory._msg_map[_msg_type](payload, to_addr)
        msg.from_addr = from_addr
        msg._validate_addresses()

        #break out the name and type, then validate
        payload_type = payload[0].split(':')[0]
        payload_name = payload[0].split(':')[1]

        if not payload_name or not payload_type:
            raise InvalidPacketError()

        if len(payload_name) > 16 or len(payload_type) > 16:
            raise InvalidPacketError()

        msg.payload.type_name = payload_type
        msg.payload.name = payload_name

        if len(payload) == 1:
            return msg

        payload = payload[1:]

        #now split out each attribute and add them to the payload as
        #attribute objects
        for attr in payload:
            if len(attr.split(':')) != 2:
                raise InvalidPacketError()

            attr_key = attr.split(':')[0]
            attr_value = attr.split(':')[1]

            if not (attr_key[0], attr_key[-1]) == ('[', ']'):
                raise InvalidPacketError()

            attr_key = attr_key[1:-1]

            msg.append_payload_attribute(Attribute(attr_key, attr_value))

        return msg

class Gateway:
    def __init__(self):
        self._sock = None
        self._shutdown = Event()
        self._qry_handlers = {}
        self._cmd_handlers = {}
        self._ann_handlers = {}
        self._ack_handlers = {}
        self._nak_handlers = {}
        self._listening = False
        self._input_buf = []
        self._current_packet = ""
        self._shutdown.clear()

        self._listen_t = None

    def _post_announcement(self, msg):

        if self._ann_handlers.has_key(msg.payload_name):
            for handler in self._ann_handlers[msg.payload_name]:
                handler(msg)

    def _post_command(self, msg):
        if self._cmd_handlers.has_key(msg.payload_name):
            for handler in self._cmd_handlers[msg.payload_name]:
                handler(msg)

    def _post_query(self, msg):
        if self._qry_handlers.has_key(msg.payload_name):
            for handler in self._qry_handlers[msg.payload_name]:
                handler(msg)

    def _post_ack(self, msg):
        if self._ack_handlers.has_key(msg.payload_name):
            for handler in self._ack_handlers[msg.payload_name]:
                handler(msg)

    def _post_nak(self, msg):
        if self._nak_handlers.has_key(msg.payload_name):
            for handler in self._nak_handlers[msg.payload_name]:
                handler(msg)

    def _handle_packet(self):
        msg = MessageFactory.make(self._current_packet)

        if not msg.to_addr in get_local_ips() and not msg.to_addr == '~':
            _log.debug("Filtering packet:{}".format(msg))
            return

        if type(msg) == Announcement:
            self._post_announcement(msg)
        elif type(msg) == Command:
            self._post_command(msg)
        elif type(msg) == Query:
            self._post_query(msg)
        elif type(msg) == Ack:
            self._post_ack(msg)
        elif type(msg) == Nak:
            self._post_nak(msg)
        else:
            print "unhandled message: {}".format(str(msg))

    # Checks for packet, if a valid packet is in the buffer, slice the
    # buffer and handle
    def _check_for_packet(self):
        if len(self._input_buf) == 0:
            _log.debug("zero length input")
            return

        #make sure we have a frame start
        start_idx = self._input_buf.index('{')

        #If there was no match for the start index, clear the current
        #buffer and return
        if start_idx == 0 and self._input_buf[0] != '{':
            self._input_buf[:] = []
            return

        #Re-frame the packet to start at the beginning of the start index
        self._input_buf = self._input_buf[start_idx:]

        # ensure that there is a sequence of charcters framed by {},
        end_idx = None
        idx = 1
        while idx < len(self._input_buf) and not end_idx:
            #This scans for an unescaped }
            if self._input_buf[idx] == '}' and self._input_buf[idx - 1] != '\\':
                end_idx = idx
            idx += 1

        #no end index in sight
        if not end_idx:
            #make sure the input buffer isn't growing too large, if it
            #is, invalidate the current buffer
            if len(self._input_buf) > 512:
                self._input_buf.pop(0)
            return

        # Set the current packet
        self._current_packet = ''.join(self._input_buf[start_idx:end_idx + 1])

        # advance the input buffer
        self._input_buf = self._input_buf[end_idx + 1:]

        # attempt to handle the current packet
        try:
            self._handle_packet()
        except Exception, err:
            print 60*'-'
            print "Unable to decode packet"
            print self._current_packet
            print 60*'-'
            traceback.print_exc(file=sys.stdout)
            print 60*'='

        #get out of here
        return

    def _listen_thread(self):
        _sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM,
                                   socket.IPPROTO_UDP)

        _sock.setblocking(0)

        _sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        _sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
        _sock.bind(('', SRV_PORT))
        mreq = struct.pack("=4sl", socket.inet_aton(SRV_MCAST_ADDR),
                           socket.INADDR_ANY)
        _sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

        while not self._shutdown.is_set():
            try:
                self._input_buf.extend(_sock.recv(512))
            except socket.error, e:
                err = e.args[0]
                if err == errno.EAGAIN or err == errno.EWOULDBLOCK:
                    sleep(1)
                    continue
                else:
                    # a "real" error occurred
                    print e
                    sys.exit(1)
            self._check_for_packet()
        _sock.close()
        _log.debug("Closing receive Socket")

    # Starts the UDP listen thread
    def start(self, allow_loopback = False):
        if self._listen_t :
            raise Exception("Already Listening")


        self._listen_t = Thread(target=self._listen_thread)

        _log.info("Starting sddcp gateway")
        self._listen_t.daemon = True
        self._listen_t.start()

    def stop(self):
        self._shutdown.set()

        self._listen_t.join()

        self._listen_t = None

    def register_announcement_handler(self, payload_type, handler):
        _log.debug("Registering Announcement handler for: {}".format(payload_type))
        if not self._ann_handlers.has_key(payload_type):
            self._ann_handlers[payload_type] = []
        if not handler in self._ann_handlers[payload_type]:
            self._ann_handlers[payload_type].append(handler)

    def register_command_handler(self, payload_type, handler):
        _log.debug("Registering Command handler for: {}".format(payload_type))
        if not self._cmd_handlers.has_key(payload_type):
            self._cmd_handlers[payload_type] = []
        if not handler in self._cmd_handlers[payload_type]:
            self._cmd_handlers[payload_type].append(handler)
        
    def register_ack_handler(self, payload_type, handler):
        _log.debug("Registering Ack handler for: {}".format(payload_type))
        if not self._ack_handlers.has_key(payload_type):
            self._ack_handlers[payload_type] = []
        if not handler in self._ack_handlers[payload_type]:
            self._ack_handlers[payload_type].append(handler)
        
    def register_nak_handler(self, payload_type, handler):
        _log.debug("Registering Nak handler for: {}".format(payload_type))
        if not self._nak_handlers.has_key(payload_type):
            self._nak_handlers[payload_type] = []
        if not handler in self._nak_handlers[payload_type]:
            self._nak_handlers[payload_type].append(handler)
        
    def register_query_handler(self, payload_type, handler):
        _log.debug("Registering Query handler for: {}".format(payload_type))
        if not self._qry_handlers.has_key(payload_type):
            self._qry_handlers[payload_type] = []
        if not handler in self._qry_handlers[payload_type]:
            self._qry_handlers[payload_type].append(handler)
