import sddcp
import unittest
from threading import Event
import logging

class SDDCPTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        sddcp.set_log_verbosity(logging.DEBUG)

    def setUp(self):
        self.gate = sddcp.Gateway()
        self.gate.start()

    def tearDown(self):
        self.gate.stop()

    def test_announce_decode(self):
         ann = "{sddcp1*ann(192.168.32.1|~)=<svc:mqtt|[data]:temperature>}"
         msg = sddcp.MessageFactory.make(ann)
         self.assertEqual(type(msg), sddcp.Announcement)

    def test_query_decode(self):
        qry = "{sddcp1*qry(192.168.32.1|~)=<svc:tftp>}"
        msg = sddcp.MessageFactory.make(qry)
        self.assertEqual(type(msg), sddcp.Query)

    def test_command_decode(self):
        cmd = "{sddcp1*cmd(192.168.32.1|192.168.32.2)=<com:mqtt_st|[port]:1334>}"
        msg = sddcp.MessageFactory.make(cmd)
        self.assertEqual(type(msg), sddcp.Command)

    def test_ack_decode(self):
        ack = "{sddcp1*ack(192.168.32.1|192.168.32.2)=<rsp:mqtt>}"
        msg = sddcp.MessageFactory.make(ack)
        self.assertEqual(type(msg), sddcp.Ack)

    def test_nak_decode(self):
        nak = "{sddcp1*nak(192.168.32.1|192.168.32.1)=<rsp:mqtt>}"
        msg = sddcp.MessageFactory.make(nak)
        self.assertEqual(type(msg), sddcp.Nak)

    def test_announce_handling(self):
        announce_received = Event()
        def _announce_handler(msg):
            self.assertEqual(msg.payload_name, 'mqtt')
            announce_received.set()

        self.gate.register_announcement_handler('mqtt', _announce_handler)

        ann = sddcp.Announcement(payload_name='mqtt',
                                 to_addr=sddcp.ADDR_ALL)
        ann.send()

        announce_received.wait(10)
        self.assertEqual(announce_received.is_set(), True)

    def test_command_handling(self):
        command_received = Event()
        def _command_handler(msg):
            self.assertEqual(msg.payload_name, 'mqtt_start')
            command_received.set()

        self.gate.register_command_handler('mqtt_start', _command_handler)

        cmd = sddcp.Command(payload_name='mqtt_start',
                            to_addr=sddcp.ADDR_ALL)
        cmd.send()

        command_received.wait(10)
        self.assertEqual(command_received.is_set(), True)

    def test_ack_handling(self):
        ack_received = Event()
        def _ack_handler(msg):
            self.assertEqual(msg.payload_name, 'mqtt_start')
            ack_received.set()

        self.gate.register_ack_handler('mqtt_start', _ack_handler)

        ack = sddcp.Ack(payload_name='mqtt_start',
                        to_addr=sddcp.ADDR_ALL)
        ack.send()

        ack_received.wait(10)
        self.assertEqual(ack_received.is_set(), True)

    def test_nak_handling(self):
        nak_received = Event()
        def _nak_handler(msg):
            self.assertEqual(msg.payload_name, 'mqtt_start')
            nak_received.set()

        self.gate.register_nak_handler('mqtt_start', _nak_handler)

        nak = sddcp.Nak(payload_name='mqtt_start',
                        to_addr=sddcp.ADDR_ALL)
        nak.send()

        nak_received.wait(10)
        self.assertEqual(nak_received.is_set(), True)

    def test_query_handling(self):
        query_received = Event()
        def _query_handler(msg):
            self.assertEqual(msg.payload_name, 'mqtt')
            query_received.set()

        self.gate.register_query_handler('mqtt', _query_handler)

        query = sddcp.Query(payload_name='mqtt',
                            to_addr=sddcp.ADDR_ALL)
        query.send()

        query_received.wait(10)
        self.assertEqual(query_received.is_set(), True)

    def test_double_start(self):
        error_thrown = False

        try:
            self.gate.start()
        except:
            error_thrown = True

        self.assertTrue(error_thrown)

    def test_invalid_packet_type(self):
        exception_thrown = False

        nak = "{sddcp1*nack(192.168.32.1|192.168.32.1)=<rsp:mqtt>}"
        try:
            msg = sddcp.MessageFactory.make(nak)
        except sddcp.InvalidPacketError as e:
            exception_thrown = True

        self.assertTrue(exception_thrown)

    def test_malformed_attribute(self):
        exception_thrown = False
        ann = "{sddcp1*ann(192.168.32.1|~)=<svc:mqtt|[data:temperature>}"
        try:
            msg = sddcp.MessageFactory.make(ann)
        except sddcp.InvalidPacketError as e:
            exception_thrown = True

        self.assertTrue(exception_thrown)

    def test_attribute_creation(self):
        exception_thrown = False
        ann = "{sddcp1*ann(192.168.32.1|~)=<svc:mqtt|[supercalifragilisticexpialadocious]:foobar>}"
        try:
            msg = sddcp.MessageFactory.make(ann)
        except sddcp.InvalidAttributeError as e:
            exception_thrown = True

        self.assertTrue(exception_thrown)
            
        
